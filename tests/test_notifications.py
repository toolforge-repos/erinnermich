from erinnermichbot.notifications import Notifications
from erinnermichbot.notification import Notification
import unittest


class Test_Notifications(unittest.TestCase):
    def test_add(self):
        objnots = Notifications
        obj = Notification({'revid': 249289827, 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'})
        objnots.add(objnots, obj)
        self.assertEqual(objnots.notifications, {obj.revid: obj})

    def test_delete(self):
        objnots = Notifications
        obj1 = Notification({'revid': 249289827, 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'})
        obj2 = Notification({'revid': 249289828, 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'})
        objnots.add(objnots, obj1)
        objnots.add(objnots, obj2)
        self.assertEqual(objnots.notifications, {obj1.revid: obj1, obj2.revid: obj2})
        objnots.delete(objnots, obj1)
        self.assertEqual(objnots.notifications, {obj2.revid: obj2})

    def test_read_notifications(self):
        obj = Notification(
            {'revid': 249289827, 'user': 'Mary Joanna', 'date': '12.10.1337', 'title': 'User talk:Mary Joanna'})
        objnots = Notifications()
        objnots.read_notifications()
        self.assertEqual(objnots.notifications[int(obj.revid)].get_dict(), obj.get_dict())

    def test_write_notifications(self):
        pass

    def test_get_last_ten_notifications(self):
        obj1 = Notification(
            {'revid': 249289827, 'user': 'Mary Joanna', 'date': '12.10.1337', 'title': 'User talk:Mary Joanna'})
        obj2 = Notification(
            {'revid': 1337, 'user': 'Mary Joanna', 'date': '12.12.2444', 'title': 'User talk:Mary Joanna'})
        objnots = Notifications()
        self.assertEqual(len(objnots.get_last_ten_notifications()), 2)

    def test_get_notifications_to_send(self):
        obj1 = Notification(
            {'revid': 249289827, 'user': 'Mary Joanna', 'date': '12.10.1337', 'title': 'User talk:Mary Joanna'})
        obj2 = Notification(
            {'revid': 1337, 'user': 'Mary Joanna', 'date': '12.12.2444', 'title': 'User talk:Mary Joanna'})
        objnots = Notifications()
        self.assertEqual(len(objnots.get_notifications_to_send()), 1)
