# Tests mit pytest
werden automatisch durch Gitlab bei jedem Push ausgeführt

weitere Dokumentation:
[https://docs.pytest.org/en/stable/](https://docs.pytest.org/en/stable/)

lokale Ausführung:
```
cd tests
pip install -r requirements.txt
pytest
```
