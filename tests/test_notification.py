from erinnermichbot.notification import Notification
import unittest

class Test_Notification(unittest.TestCase):
    def test_set_date(self):
        self.assertEqual(Notification({'revid': '249289827', 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'}).set_date('08.10.2024').get_dict(), {'revid': '249289827', 'user': 'Mary Joanna', 'date': '08.10.2024', 'title': 'User talk:Mary Joanna'})

    def test_get_dict(self):
        self.assertEqual(Notification({'revid': '249289827', 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'}).get_dict(), {'revid': '249289827', 'user': 'Mary Joanna', 'date': '?', 'title': 'User talk:Mary Joanna'})

    def test_valid(self):
        self.assertEqual(bool(Notification({'revid': '249289827', 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'})), False)
        self.assertEqual(bool(Notification({'revid': '249289827', 'user': 'Mary Joanna', 'title': 'User talk:Mary Joanna'}).set_date('08.10.2024')), True)