from erinnermichbot import Receiver
from erinnermichbot.receiver import try_parsing_date
from erinnermichbot.notification import Notification
import unittest
import locale

def test_try_parsing_date():
    try:
        locale.setlocale(locale.LC_TIME, "de_DE")
        assert try_parsing_date('8. Oktober 2024').strftime('%d.%m.%Y') == '08.10.2024'
    except locale.Error:
        assert try_parsing_date('8. October 2024').strftime('%d.%m.%Y') == '08.10.2024'
        pass
    assert try_parsing_date('8.10.2024').strftime('%d.%m.%Y') == '08.10.2024'
    assert try_parsing_date('8.10.24').strftime('%d.%m.%Y') == '08.10.2024'

class Test_Receiver(unittest.TestCase):
    def test_get_date_bytext1(self):
        receiver = Receiver()
        obj = Notification({'revid': 250959033, 'user': 'Mary Joanna', 'title': 'User:Mary Joanna/Spielwiese'})
        receiver.get_date_bytext(obj)
        self.assertEqual(obj.date, '12.12.2024')

    def test_get_date_bytext2(self):
        receiver = Receiver()
        obj = Notification({'revid': 250959046, 'user': 'Mary Joanna', 'title': 'User:Mary Joanna/Spielwiese'})
        receiver.get_date_bytext(obj)
        self.assertEqual(obj.date, '11.12.2024')

    def test_get_date_bytext3(self):
        receiver = Receiver()
        obj = Notification({'revid': 250959056, 'user': 'Mary Joanna', 'title': 'User:Mary Joanna/Spielwiese'})
        receiver.get_date_bytext(obj)
        self.assertEqual(obj.date, '13.12.2024')