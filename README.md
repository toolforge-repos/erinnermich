Der **ErinnerMichBot** ist ein Tool zur Wiedervorlage von Wikipedia-Seiten. Erinnerungswünsche können von jedem angemeldeten Nutzer auf beliebigen Diskussionsseiten hinterlassen werden. Der Bot ergänzt diese Wünsche in einem zentralen Logbuch und erinnert den Nutzer dann am gewünschten Datum direkt auf der ursprünglichen Diskussionsseite.

# So funktioniert's

## (1) Erinnerungswunsch auf Diskussionsseite anfügen

Der Bot reagiert auf Erwähnungen über die Vorlage ```{{ErinnerMich|tt.mm.jjjj}}```. Damit das Echo-System eine Erwähnung auslöst, muss diese Vorlage auf einer Diskussionsseite eingefügt werden (niemals direkt im Artikel-Namensraum!) und der Beitrag eine Unterschrift ```~~~~``` enthalten.

### Beispiele
* ```{{ErinnerMich|24.12.2021}} Erinner mich an Weihnachten! --~~~~```
* ```{{ErinnerMich|02.06.2022}} Hochzeitstag nicht vergessen! --~~~~```

## (2) Benachrichtigung prüfen

Hat alles geklappt, wird sich der ErinnerMichBot nach einigen Sekunden per Benachrichtigung melden:

![Bestätigung des ErinnerMichBot](../ErinnerMichBot_Anfrage_gesendet.png)

## (3) Auf Erinnerung warten

Am Wunschdatum wird der ErinnerMichBot die ursprüngliche Diskussionsseite besuchen und von dort erneut eine Benachrichtigung senden:

![Erinnerung des ErinnerMichBot](../ErinnerMichBot_Erinnerung_gesendet.png)

# Häufig gestellte Fragen

## Kann ich Erinnerungswünsche nachträglich ändern oder löschen?

Momentan noch nicht: Ist der Wunsch vom Bot erfasst, wird die Erinnerung auch gesendet. Es spielt dabei keine Rolle, ob der ursprüngliche Wunsch zum Zeitpunkt der Erinnerung noch auf der Diskussionsseite steht oder nicht.

## Kann ich mir regelmäßig wiederkehrende Erinnerungen wünschen?

Momentan noch nicht. Ist aber in Planung, sobald ich eine gute Lösung für das Löschen von Wünschen gefunden habe.

# Technische Infos
* ErinnerMich @ Toolhub: [https://toolhub.wikimedia.org/tools/toolforge-erinnermich](https://toolhub.wikimedia.org/tools/toolforge-erinnermich)
* Quellcode bei Gitlab: [https://gitlab.wikimedia.org/toolforge-repos/erinnermich](https://gitlab.wikimedia.org/toolforge-repos/erinnermich)
* Dokumentation bei Readthedocs:  [https://erinnermichbot.readthedocs.io/en/latest/index.html](https://erinnermichbot.readthedocs.io/en/latest/index.html)