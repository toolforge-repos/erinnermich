.. include:: ../README.md
   :parser: myst_parser.sphinx_

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contents:

   erinnermichbot/receiver
   erinnermichbot/reminder
   erinnermichbot/common
   erinnermichbot/notifications
   erinnermichbot/notification
   erinnermichbot/session