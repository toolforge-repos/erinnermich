import yaml
from datetime import datetime

from erinnermichbot.common import Common
from erinnermichbot.notification import Notification


class Notifications:
    """
    this class represents log.yaml and holds all notifications
    """

    common = Common()
    """common class"""

    notifications = {}
    """the dictionary of notifications. key is revid"""

    def __init__(self):
        self.read_notifications()

    def add(self, obj: Notification):
        """
        add new notification to self.notifications

        Parameters
        ----------
        obj:
            a Notification
        """
        self.common.logger.info('add notification: ' + str(obj.get_dict()))
        if not isinstance(obj, Notification):
            raise TypeError
        self.notifications[obj.revid] = obj

    def delete(self, obj: Notification):
        """
        remove notification

        Parameters
        ----------
        obj:
            a Notification
        """
        self.common.logger.info('delete notification: ' + str(obj.get_dict()))
        if not isinstance(obj, Notification):
            raise TypeError
        del self.notifications[obj.revid]

    def read_notifications(self):
        """
        Read previously processed and logged entries
        """
        self.notifications = {}
        self.common.logger.debug('read notifications from ' + self.common.config['WP_BOT_LOG'] + ': ')
        with open(self.common.config['WP_BOT_LOG'], 'r') as stream:
            n = yaml.safe_load(stream)
            stream.close()

        if n is not None:
            for e in n:
                obj = Notification(e)
                self.common.logger.debug('read notification: ' + str(obj.get_dict()))
                self.notifications[obj.revid] = obj

    def write_notifications(self):
        """
        write all valid entrys to file
        """
        self.common.logger.info('write notifications to ' + self.common.config['WP_BOT_LOG'] + ': ')
        l = []
        for obj in self.notifications.values():
            if obj:
                l.append(obj.get_dict())
                self.common.logger.debug('write notification: ' + str(obj.get_dict()))
        with open(self.common.config['WP_BOT_LOG'], 'w') as outfile:
            yaml.dump(l, outfile)
            outfile.close()

    def get_last_ten_notifications(self):
        """
        Returns
        -------
        list:
            return last 10 notifications
        """
        return list(self.notifications.values())[-10:]

    def get_notifications_to_send(self):
        """
        Returns
        -------
        list:
            return all notifications, which date is not in the future
        """
        r = []
        for obj in self.notifications.values():
            if obj and datetime.strptime(obj.date, '%d.%m.%Y') <= datetime.now():
                r.append(obj)
        return r