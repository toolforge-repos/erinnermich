import mwapi
from time import sleep


class Session:
    """
    proxy class for session api requests

    File "/data/project/EinnerMichModul/home/receive_requests.py", line 33, in <module>
    mwapi.errors.ConnectionError: ('Connection aborted.', RemoteDisconnected('Remote end closed connection without response'))

    File "/data/project/EinnerMichModul/home/receive_requests.py", line 33, in <module>
    ValueError: Could not decode as JSON:
    <!DOCTYPE html>
    <html lang="en">
    <meta charset="utf-8">
    <title>Wikimedia Error</title>
    <style>

    in the err log this two errors are really often. so we catch them and try again after one minute

    Parameters
    ----------
    common:
        needs the Common class
    """
    def __init__(self, common):
        self.session = mwapi.Session('https://' + common.config['WP_BOT_WIKI'],
                                     user_agent="ErinnerMichBot by User:Tkarcher")
        self.common = common

    def request(self, foo, *args, **kwargs):
        """
        runs the request

        Parameters
        ----------
        foo:
            get or post
        args:
            *args from parent
        kwargs:
            **kwargs from parent

        Returns
        -------
        str:
            A response JSON documents

        Raises
        ------
        NotImplementedError
            Raised if another proxy get called
        TimeoutError
            Raised if after 12 hours we didn't success
        """
        if foo == 'session.get':
            func = self.session.get
        elif foo == 'session.post':
            func = self.session.post
        else:
            raise NotImplementedError('session proxy class support only get and post')

        api_attempt = 0
        while True:
            try:
                # we should respect ratelimit
                sleep(60 // int(self.common.config['API_RATELIMIT']))
                output = func(*args, **kwargs)
            except (mwapi.errors.ConnectionError, ValueError) as err:
                self.common.logger.error(f"Unexpected {err=}, {type(err)=}")
                sleep(60)
                # break out this loop after 1800 iterations
                api_attempt += 1
                if api_attempt > 1800:
                    self.common.logger.error('interrupt the loop after 12 hours')
                    raise TimeoutError('interrupt the loop after 12 hours')
                continue
            else:
                return output

    def get(self, *args, **kwargs):
        """
        get proxy

        Parameters
        ----------
        args:
            *args from parent
        kwargs:
            **kwargs from parent

        Returns
        -------
        str:
            A response JSON documents
        """
        return self.request('session.get', *args, **kwargs)

    def post(self, *args, **kwargs):
        """
        post proxy

        Parameters
        ----------
        args:
            *args from parent
        kwargs:
            **kwargs from parent

        Returns
        -------
        str:
            A response JSON documents
        """
        return self.request('session.post', *args, **kwargs)
