#from erinnermichbot.common import Common


class Notification:
    """
    this class represents a notification itself

    Parameters
    ----------
    params:
        a dictionary with revid, user, date and title
    """
    revid = None
    """revisions id (`int`)"""
    user = None
    """wikipedia username (`str`)"""
    date = None
    """date from {{ErinnerMich|<date>}} (`str`)"""
    title = None
    """the target wiki page (`str`)"""
    valid = None
    """true, if date is valid (`bool`)"""

    def __init__(self, params={}):
        """
        set params by a dictionary

        Parameters
        ----------
        params:
            dictionary with revid, user, title and date
        """
        #self.common = Common()

        self.revid = 0
        self.user = ''
        self.date = '?'
        self.title = ''
        # used at send_reminders.py, try more the one time to send reminder. TODO: review after some time
        self.send_attempt = 0
        self.valid = False

        # new notifications from mentions have no date. so this would be an KeyError, do nothing
        try:
            self.revid = params['revid']
            self.user = params['user']
            self.title = params['title']
            self.set_date(params['date'])
        except KeyError:
            pass

    def set_date(self, date: str | None = None):
        """
        set date

        Parameters
        ----------
        date:
             valid date or ?
        """
        if date and date != '?':
            self.date = date
            self.valid = True
        else:
            self.date = '?'
            self.valid = False

        return self

    def get_dict(self):
        """
        returns a dictionary

        Returns
        -------
        dict:
            return a dictionary with revid, user, date and title
        """
        return {'revid': self.revid, 'user': self.user, 'date': self.date, 'title': self.title}

    def __bool__(self):
        """
        Returns
        -------
        bool:
            is this notification valid? have a correct date?
        """
        return self.valid
