# coding=utf-8
# notifications bot running in notifications.wikipedia.org

import re
from datetime import datetime
from urllib.parse import quote
from time import sleep
from erinnermichbot.common import Common
from erinnermichbot.notifications import Notifications
from erinnermichbot.notification import Notification
import locale

try:
    locale.setlocale(locale.LC_TIME, "de_DE")
except locale.Error:
    pass

def try_parsing_date(text):
    """
    try different formats seen in public to get the date
    """
    for fmt in ('%d.%m.%Y', '%d.%m.%y', '%d. %B %Y'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError('no valid date format found')


class Receiver:
    """
    Receiver class

    get by api the new notifications
    """
    common = Common('receive_requests')
    """common class with logging_script='receive_requests'"""

    notifications: Notifications
    """notifications class"""

    def __init__(self):
        self.common.login()
        self.notifications = Notifications()

    def run_bot(self):
        """
        runs the bot in a endless loop
        gets new notificications and process them by self.process_notifications(notifications)
        """
        self.common.logger.info('started bot receive_requests')
        while True:
            notifications = self.common.session.get(action='query',
                                                    meta='notifications',
                                                    notfilter='!read')['query']['notifications']['list']
            if notifications:
                self.common.logger.info('got new notifications from '
                                         + self.common.config['WP_BOT_WIKI'] + str(notifications))
            self.process_notifications(notifications)

            # Mark processed notifications as read
            if notifications:
                csrftoken = self.common.session.get(action='query',
                                                    meta='tokens')['query']['tokens']['csrftoken']
                self.common.session.post(action='echomarkread',
                                         list='|'.join([str(n['id']) for n in notifications]),
                                         token=csrftoken)

            # wait 10 seconds
            sleep(5)

    def get_date_bytext(self, obj: Notification):
        """
        try to get the date of notification by api

        Parameters
        ----------
        obj:
            a Notification
        """
        self.common.logger.info('try to fetch date for: ' + str(obj.get_dict()))

        ### Solution1 by query
        revdata = self.common.session.get(action='query',
                              prop='revisions',
                              titles=obj.title,
                              rvprop='content',
                              rvslots='main',
                              rvlimit=2,
                              rvstartid=obj.revid)['query']['pages']
        revs = list(revdata.values())[0]['revisions']
        curr = revs[0]['slots']['main']['*'].split('\n')
        # Delete previously existing lines from content if there are any
        if len(revs) > 1:
            prev = set(revs[1]['slots']['main']['*'].split('\n'))
            text_added = [line for line in curr if line not in prev]
        else:
            text_added = curr
        ### /Solution1

        ### Solution2 by compare
        """
        diff = self.common.session.get(action='compare',
                                       prop='diff',
                                       fromrev=obj.revid,
                                       torelative='prev',
                                       difftype='unified')['compare']['*']
        self.common.logger.info('diff: ' + str(diff))

        # extract the added text
        pattern_added = re.compile(
            r'^\+(.*)',
            re.IGNORECASE | re.MULTILINE
        )
        text_added = pattern_added.findall(diff)
        """
        ### /Solution2 by compare

        self.common.logger.info('added text: ' + str(text_added))

        # Regex pattern to extract date from {{ErinnerMich|<date>}} in wikitext
        date_pattern = re.compile(r'\{\{ErinnerMich\|([^}]+)\}\}', re.IGNORECASE)
        dates_added = date_pattern.findall(''.join(text_added))

        if not dates_added:
            self.common.logger.info('found none ErinnerMich template')
        else:
            self.common.logger.info('found this dates: ' + str(dates_added))

            # try to get dates for all found date strings
            dates = []
            for i in dates_added:
                try:
                    dates.append(try_parsing_date(i))
                except ValueError:
                    self.common.logger.warning(i + ' is n\'t a valid date')
                    pass

            # sort the list, the date in the latest future will be the first
            dates.sort(reverse=True)

            # set date at notification
            obj.set_date(dates[0].strftime('%d.%m.%Y'))

    def update_wikilog(self, latestobj: Notification):
        """
        update the logbuch on https://de.wikipedia.org/wiki/Benutzer:ErinnerMichBot/Logbuch

        Parameters
        ----------
        latestobj:
            the latest Notification for pinging the user
        """
        wikitext = "== Letzte 10 empfangene Erinnerungswünsche ==\n"

        for obj in self.notifications.get_last_ten_notifications():
            if obj:
                wikitext += (
                    "* [[User:{u}|{u}]] wünscht sich eine Erinnerung auf "
                    "[[{t}]] ([https://{wiki}/w/index.php?title="
                    "{qt}&diff=prev&oldid={r} diff]) am {d}.\n".format(
                        u=obj.user,
                        t=obj.title,
                        qt=quote(obj.title),
                        r=obj.revid,
                        d=obj.date,
                        wiki=self.common.config['WP_BOT_WIKI']))
            else:
                wikitext += (
                    "* [[User:{u}|{u}]] hat mich [https://{wiki}"
                    "/w/index.php?title={qt}&diff=prev&oldid={r} erwähnt],"
                    " aber ohne gültige ErinnerMich-Vorlage. :-(\n".format(
                        u=obj.user,
                        qt=quote(obj.title),
                        r=obj.revid,
                        wiki=self.common.config['WP_BOT_WIKI']))

        wikicommment = '[[User:{u}|{u}]]: {m}'.format(
            u=latestobj.user,
            m='Klar, mach ich gern.' if latestobj else
            'Tut mir leid, das hab ich nicht verstanden. Bitte das Datum korrekt angeben')
        self.common.logger.info('add a new entry to the Logbuch: ' + str(latestobj.get_dict()))

        # Add new entry to public log
        csrftoken = self.common.session.get(action='query',
                                            meta='tokens')['query']['tokens']['csrftoken']
        self.common.session.post(action='edit',
                                 title='User:ErinnerMichBot/Logbuch',
                                 text=wikitext,
                                 summary=wikicommment,
                                 bot=1,
                                 token=csrftoken)

    def process_notifications(self, notifications: list):
        """
        process the notifications from mediawiki api

        Parameters
        ----------
        notifications:
            the latest mentions
        """
        mentions = [n for n in notifications if n['type'] == 'mention']
        if mentions:
            entries = [{'user': m['agent']['name'],
                        'revid': m['revid'],
                        'title': m['title']['full']} for m in mentions]

            # Parse and process all new entries
            for e in entries:
                self.common.logger.info('process mention: ' + str(e))
                obj = Notification(e)
                # setting the date need extra api request:
                self.get_date_bytext(obj)

                """ 
                Add processed entry to changelog and file 
                first we read the file again, cause send_reminders.py may deleted an entry
                """
                self.notifications.read_notifications()
                self.notifications.add(obj)
                self.notifications.write_notifications()

                # update the public logbuch
                self.update_wikilog(obj)