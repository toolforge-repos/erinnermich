import logging
import configparser
from os import getenv
from erinnermichbot.session import Session


class Common:
    """
    read config, set logger and proxy session

    Parameters
    ----------
    logging_script:
        name of calling script, important for logging
    """

    logger: logging.Logger
    """logger for using in all other classes"""

    session: Session
    """proxy session class"""

    def __init__(self, logging_script=''):
        self.logging_script = logging_script

        # read config from /data/project/erinnermich/erinnermich.ini
        config = configparser.ConfigParser()
        config.read('erinnermich.ini')

        # set environment. if non given standard is production
        if getenv('WP_BOT_ENVIRONMENT'):
            self.config = config[getenv('WP_BOT_ENVIRONMENT')]
            self.config['WP_BOT_ENVIRONMENT'] = getenv('WP_BOT_ENVIRONMENT')
        else:
            self.config = config['production']
            self.config['WP_BOT_ENVIRONMENT'] = 'production'

        log_level_info = {
            'logging.DEBUG': logging.DEBUG,
            'logging.INFO': logging.INFO,
            'logging.WARNING': logging.WARNING,
            'logging.ERROR': logging.ERROR,
        }

        # we want a logfile with usefull informations like class + function and the time
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(
            filename=self.config['WP_BOT_LOGFILE'],
            encoding='utf-8',
            level=log_level_info.get(self.config['WP_BOT_LOGLEVEl'], logging.ERROR),
            format='%(asctime)s:%(levelname)s:' + self.logging_script + ':%(module)s:%(funcName)s:%(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )
        self.session = Session(self)

    def login(self):
        """
        do session login

        Login using bot password with the workaround described in
        https://github.com/mediawiki-utilities/python-mwapi/issues/35
        """
        self.logger.info('api login on ' + self.config['WP_BOT_WIKI'] + ' as ' + self.config['WP_BOT_USER'])
        lgtoken = self.session.get(action='query',
                                   meta='tokens',
                                   type='login')['query']['tokens']['logintoken']
        self.session.post(action='login',
                          lgname=self.config['WP_BOT_USER'],
                          lgpassword=self.config['WP_BOT_PWD'],
                          lgtoken=lgtoken)