# coding=utf-8
# notifications bot running in notifications.wikipedia.org

import mwapi
from datetime import datetime as dt

from erinnermichbot.common import Common
from erinnermichbot.notifications import Notifications


class Reminder:
    """
    Receiver class

    get by api the new notifications
    """
    common = Common('send_reminders')
    """common class with logging_script='send_reminders'"""

    notifications: Notifications
    """notifications class"""

    def __init__(self):
        self.common.login()
        self.notifications = Notifications()

    def run_bot(self):
        """
        process all ready notifications. if notification could send, remove them
        """
        self.common.logger.info('started bot send_reminders')
        for obj in self.notifications.get_notifications_to_send():
            self.common.logger.info('process notification: ' + str(obj.get_dict()))
            # Add a comment to the page and ping via edit summary
            wikitext = ('\n<!-- ErinnerMichBot war hier am {d}.'
                        ' Falls dich dieser Kommentar stört,'
                        ' darfst du ihn gerne wieder löschen. -->').format(
                d=dt.strftime(dt.now(), "%d.%m.%Y"))
            wikicomment = ('Hey [[User:{u}|{u}]], hier ist deine'
                           ' [[Special:Diff/{r}|damals gewünschte]]'
                           ' Erinnerung. VG, ErinnerMichBot'.format(
                u=obj.user,
                r=obj.revid))
            try:
                csrftoken = self.common.session.get(action='query',
                                                    meta='tokens')['query']['tokens']['csrftoken']
                self.common.session.post(action='edit',
                                         title=obj.title,
                                         appendtext=wikitext,
                                         summary=wikicomment,
                                         minor=1,
                                         nocreate=1,
                                         redirect=1,
                                         bot=1,
                                         token=csrftoken)
            except mwapi.errors.APIError:
                errmsg = ('\n* Die von [[User:{u}|{u}]] [[Special:Diff/{r}|'
                          'hier]] gewünschte Erinnerung konnte nicht gesendet'
                          ' werden.'.format(
                    u=obj.user,
                    r=obj.revid))
                self.common.logger.error('process notification failed: ' + errmsg)
                csrftoken = self.common.session.get(action='query',
                                                    meta='tokens')['query']['tokens']['csrftoken']
                self.common.session.post(action='edit',
                                         title='User:ErinnerMichBot/Meldungen',
                                         appendtext=errmsg,
                                         summary='Erinnerung fehlgeschlagen',
                                         minor=1,
                                         nocreate=1,
                                         redirect=1,
                                         bot=1,
                                         token=csrftoken)

                """ 
                count send_attempt up, if 5 times failed, delete. 
                TODO: maybe this is useless code, review later, if there are log entries
                """
                obj.send_attempt += 1
                if obj.send_attempt > 5:
                    self.common.logger.info('failed to send notification at 5 times, now delete: ' + str(obj.get_dict))
                    self.notifications.delete(obj)
            else:
                # remove notification when its could send
                self.notifications.delete(obj)

            self.notifications.write_notifications()